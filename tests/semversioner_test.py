import unittest
from semversioner.cli import *
import shutil
import os
import tempfile
import json


class MyTestCase(unittest.TestCase):
    directory_name = None
    changes_dirname = None
    next_release_dirname = None

    @classmethod
    def setUpClass(cls):
        print("setUpClass")

    @classmethod
    def tearDownClass(cls):
        print("tearDownClass")

    def setUp(self):
        self.directory_name = tempfile.mkdtemp()
        self.changes_dirname = os.path.join(self.directory_name, '.changes')
        self.next_release_dirname = os.path.join(self.changes_dirname, 'next-release')
        print("Created directory: " + self.directory_name)

    def tearDown(self):
        print("Removing directory: " + self.directory_name)
        shutil.rmtree(self.directory_name)

    def test_increase_version(self):
        self.assertEqual(increase_version("1.0.0", "minor"), "1.1.0")
        self.assertEqual(increase_version("1.0.0", "major"), "2.0.0")
        self.assertEqual(increase_version("1.0.0", "patch"), "1.0.1")
        self.assertEqual(increase_version("0.1.1", "minor"), "0.2.0")
        self.assertEqual(increase_version("0.1.1", "major"), "1.0.0")
        self.assertEqual(increase_version("0.1.1", "patch"), "0.1.2")
        self.assertEqual(increase_version("9.9.9", "minor"), "9.10.0")
        self.assertEqual(increase_version("9.9.9", "major"), "10.0.0")
        self.assertEqual(increase_version("9.9.9", "patch"), "9.9.10")

    def test_write_new_change(self):
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        write_new_change(os.path.join(self.changes_dirname), "minor", "This is my minor description")
        write_new_change(os.path.join(self.changes_dirname), "patch", "This is my patch description")

        data = []
        files = sorted([i for i in os.listdir(self.next_release_dirname)])

        for filename in files:
            with open(os.path.join(self.next_release_dirname, filename)) as f:
                data.append(json.load(f))

        expected = [
            json.loads("""{"type": "major","description": "This is my major description"}"""),
            json.loads("""{"type": "minor","description": "This is my minor description"}"""),
            json.loads("""{"type": "patch","description": "This is my patch description"}""")
        ]

        self.assertListEqual(expected, data)

    def test_generate_changelog_empty(self):
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        write_new_change(os.path.join(self.changes_dirname), "minor", "This is my minor description")

        self.assertEqual(generate_changelog(self.changes_dirname), "# Changelog\nNote: version releases in the 0.x.y range may introduce breaking changes.\n")

    def test_generate_changelog_single_major(self):
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        release(self.changes_dirname)

        self.assertEqual(generate_changelog(self.changes_dirname),
                         """# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: This is my major description
""")

    def test_generate_changelog_single_patch(self):
        write_new_change(os.path.join(self.changes_dirname), "patch", "This is my patch description")
        release(self.changes_dirname)

        self.assertEqual(generate_changelog(self.changes_dirname),
                         """# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.1

- patch: This is my patch description
""")

    def test_generate_changelog_multiple(self):
        write_new_change(os.path.join(self.changes_dirname), "patch", "This is my patch description")
        release(self.changes_dirname)
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        release(self.changes_dirname)
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        release(self.changes_dirname)
        write_new_change(os.path.join(self.changes_dirname), "minor", "This is my minor description")
        release(self.changes_dirname)

        self.assertEqual(generate_changelog(self.changes_dirname),
                         """# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.1.0

- minor: This is my minor description

## 2.0.0

- major: This is my major description

## 1.0.0

- major: This is my major description

## 0.0.1

- patch: This is my patch description
""")

    def test_generate_changelog_multiple(self):
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        write_new_change(os.path.join(self.changes_dirname), "minor", "This is my minor description")
        write_new_change(os.path.join(self.changes_dirname), "patch", "This is my patch description")
        release(self.changes_dirname)
        write_new_change(os.path.join(self.changes_dirname), "major", "This is my major description")
        write_new_change(os.path.join(self.changes_dirname), "minor", "This is my minor description")
        write_new_change(os.path.join(self.changes_dirname), "patch", "This is my patch description")
        release(self.changes_dirname)

        self.assertEqual(generate_changelog(self.changes_dirname),
                         """# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.0.0

- major: This is my major description
- minor: This is my minor description
- patch: This is my patch description

## 1.0.0

- major: This is my major description
- minor: This is my minor description
- patch: This is my patch description
""")



if __name__ == '__main__':
    unittest.main()
